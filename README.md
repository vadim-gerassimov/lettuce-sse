# lettuce-sse

Sample spring boot project that demonstrate possible redis connection leak in combination of spring-webflux and spring data redis reactive

## How to reproduce

If you have no redis running but have docker installed being in project directory run:
```
docker-compose up -d
```

Start the demo app:
```
./gradlew bootRun
```

for windows:
```
gradlew.bat bootRun
```

First go to browser to see number of redis connections and remember the value:
```
http://localhost:8080/redis-connections
```

Next open the server sent event url in browser:
```
http://localhost:8080/events
```

You'll see pending connection but no content yet. Now send a test message by openeing:

```
http://localhost:8080/send-message
```

In events tab you should see sent message. Now refresh /redis-connections tab - number of connections should be increased by 1.

Cancel /events tab by pressinc cross icon or closing the tab.

Refresh /redis-connections tab - the number should be left unchanged.

Now send a message by refreshing /send-message tab and then refresh /redis-connections - the number of connections is still the same (because you've sent at least one message before if you cancel stream without sending a message sending a single message afterwards closes the connection).

And now send again message by refreshing /send-message. Refresh /redis-connections tab - the number of connections should decrease.