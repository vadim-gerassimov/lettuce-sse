package com.example.lettucesse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.ReactiveSubscription;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@RestController
public class Controller {

  private final ReactiveRedisOperations<String, String> redisTemplate;
  private final Logger logger = LoggerFactory.getLogger(getClass());

  public Controller(ReactiveRedisOperations<String, String> redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  @GetMapping(path = "/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<String> events() {
    return redisTemplate
        .listenToChannel("some:channel:123")
        .doOnError(t -> logger.error("Error when parsing entry", t))
        .map(ReactiveSubscription.Message::getMessage);
  }

  @GetMapping("/redis-connections")
  public Flux<Object> redisConnections() {
    return redisTemplate.execute((conn) -> conn.serverCommands().info("Clients").map(p -> p.get("connected_clients")));
  }

  @GetMapping("/send-message")
  public Mono<Long> sendMessage() {
    return redisTemplate.convertAndSend("some:channel:123", "HELLO: " + new Date());
  }
}
