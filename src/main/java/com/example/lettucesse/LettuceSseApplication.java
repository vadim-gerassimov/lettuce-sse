package com.example.lettucesse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LettuceSseApplication {

  public static void main(String[] args) {
    SpringApplication.run(LettuceSseApplication.class, args);
  }
}
